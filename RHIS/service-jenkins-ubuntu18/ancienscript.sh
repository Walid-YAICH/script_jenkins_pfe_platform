#Externaliser le nom de la machine cible
#Externaliser le port
#Simplifier $(ls | grep *.jar | head -n 1)

#Construction des livrables
mvn package -f myrhis-project -DskipTests -Dspring.profiles.active=recette

#Aller vers l'emplacement du jar et #Livraison : Transferer le jar vers le serveur cible
cd $WORKSPACE
cd myrhis-project/myrhis-planning-service/target
scp *.jar ubuntu@planning.recette.myrhis.interne.fr:/home/ubuntu/livraison


#Arreter l'application si elle est en cours d'execution
ssh ubuntu@planning.recette.myrhis.interne.fr bash +e '  
  #Arreter l'ancienne version de l'application
  fuser -v -n tcp 2004
  #Tester si l'application est en cours d'execution.
  if [ $? -eq 0 ]
   then
     #Arrêtez l'application si elle est en cours d'exécution
     kill $(fuser -v -n tcp 2004)
   else 
     echo "l application n est pas en cours d execution !"
  fi
'


#Lancer l'application 
# set +e #so "at now" will run even if java -jar fails
#ssh ubuntu@planning.recette.myrhis.interne.fr bash +e -c "'
#  cd /home/ubuntu/livraison
#  echo "java -Xms256m -Xmx800g -cp /home/ubuntu/livraison/libs -jar $(ls | grep *.jar | head -n 1) --spring.profiles.active=recette" | at now + 1 min
#'"


ssh ubuntu@planning.recette.myrhis.interne.fr bash +e '
	cd /home/ubuntu/livraison
	nohup java -Xms256m -Xmx800m -jar $(ls | grep *planning*.jar | head -n 1) --spring.profiles.active=recette > /dev/null 2>&1&
'

