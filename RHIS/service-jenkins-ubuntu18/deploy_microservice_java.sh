#Construction des livrables
mvn package -f myrhis-project -DskipTests -Dspring.profiles.active=recette

#Aller vers l'emplacement du jar
cd myrhis-project/myrhis-config-service/target/

#Livraison : Transferer le jar vers le serveur cible
scp *.jar ubuntu@config.recette.myrhis.interne.fr:/home/ubuntu/livraison

#Arreter l'application si elle est en cours d'execution
ssh ubuntu@config.recette.myrhis.interne.fr bash +e '  
  #Arreter l'ancienne version de l'application
  fuser -v -n tcp 2000
  #Tester si l'application est en cours d'execution.
  if [ $? -eq 0 ]
   then
     #Arrêtez l'application si elle est en cours d'exécution
     kill $(fuser -v -n tcp 2000)
   else 
     echo "l application n est pas en cours d execution !"
  fi
'


#Lancer l'application 
ssh ubuntu@config.recette.myrhis.interne.fr bash +e -c "'
  cd /home/ubuntu/livraison
  echo "java -Xms256m -Xmx800m -jar $(ls | grep *.jar | head -n 1) --spring.profiles.active=recette" | at now + 1 min
'"

