#!/bin/bash -ex

###################################################################
#Script Name	: Deploy fat jar(Spring boot) on EC2 instances.                                                                                     
#Description	: 
# This script is designed to run on jenkins, it will  :
# 1) Build the project
# 2) Stop the application on the target server
# 3) Empty the delivery folder on the target machine (make sure you have a backup of this jar)
# 4) Copy the jar to the target server
# 5) Start the application on the target server
#Args           : no args.                                                                                          
#Author       	: Walid YAICH                                                
#Email         	: walid.yaich@gmail.com
#NOTE  			: Please read the documentation at the end of this script
###################################################################

ENVIRONMENT=recette
ROOT_PROJECT_FOLDER=myrhis-project #This is relative to the jenkins current workspace, this is where you find pom.xml
GENERATED_JAR_PATH=myrhis-project/myrhis-config-service/target/ #Where the jar is generated
REGEX_TO_MATCH_JAR_NAME='.*config.*.jar$'
USER_HOSTNAME_OF_TARGET_SERVER=ubuntu@config.recette.myrhis.interne.fr
TARGET_PORT=2000
PATH_DELIVERY_ON_TARGET_SERVER=/home/ubuntu/livraison #Path on the target server, where your jar will be copied
MAX_JAVA_HEAP_SIZE=80m

#1) Build
mvn package -f $ROOT_PROJECT_FOLDER -DskipTests -Dspring.profiles.active=$ENVIRONMENT

#Go to the folder which contains the generated jar
cd $GENERATED_JAR_PATH

#Get the right jar name
JAR_FILE_NAME=$(ls | grep -E "$REGEX_TO_MATCH_JAR_NAME")
echo $JAR_FILE_NAME


#2) Stop the application on the target server (if running)
#+e : to not to quit job when "fuser -v -n tcp $PORT" return error code different from 0
#     because when application is not running "fuser -v -n tcp $PORT" will return error code different from 0 
ssh $USER_HOSTNAME_OF_TARGET_SERVER "bash +e -sx $TARGET_PORT" <<'EOF'
	PORT=$1 #the argument 1 passed to this script is $TARGET_PORT
	echo 'Trying to stop the application ....'
	fuser -v -n tcp -k $PORT   # -k means kill
	if [ $? -eq 0 ]
		then
			echo 'Application stopped successfully !'
		else 
			echo 'Cannot stop the application, maybe the application is not running.'
  	fi
EOF


# 3) Empty the delivery folder on the target machine
# Note : make sure you have a backup of this jar
ssh $USER_HOSTNAME_OF_TARGET_SERVER "bash -esx $PATH_DELIVERY_ON_TARGET_SERVER" <<'EOF'
	PATH_DELIVERY=$1 #the argument 1 passed to this script is $PATH_DELIVERY_ON_TARGET_SERVER
	cd $PATH_DELIVERY
	ls -al 	#show delivery folder content
	rm -rf *  #empty the delivery folder
EOF


#4) Delivery : copy the jar to the target server
# this command looks like : 
scp $JAR_FILE_NAME $USER_HOSTNAME_OF_TARGET_SERVER:$PATH_DELIVERY_ON_TARGET_SERVER


#5) Run the application on the target server
ssh $USER_HOSTNAME_OF_TARGET_SERVER "bash -esx $PATH_DELIVERY_ON_TARGET_SERVER $JAR_FILE_NAME $ENVIRONMENT $MAX_JAVA_HEAP_SIZE" <<'EOF'
	PATH_DELIVERY=$1
    JAR_NAME=$2
    ENV=$3
    MAX_MEMORY=$4
    cd $PATH_DELIVERY
	screen -dm java -Xms256m -Xmx$MAX_MEMORY -jar $JAR_NAME --spring.profiles.active=$ENV
    echo "please check the documentation at the end of this script to know how to debug !"
EOF


######### HOW TO DEBUG #############################################################
#1) First of all, you need to login into your target server
#2) check if the application is running : "ps -ef | grep java" or "fuser -v -n tcp portNumber", 
#	if the application is running check application logs.
#3) if you cannot find any java process running or you can't find logs, please try to run yourself on the target server :
#	screen -dLm java -Xms256m -Xmx80m -jar myrhis-config-service-0.0.1-SNAPSHOT.jar --spring.profiles.active=recette
#	I added -L to enable logging for screen command.
#	I cannot enable it in the script, because, for my application, 
#	logging is configured to write in a specific file).
#4)then check the logfile "screenlog.0" (you should find it where you run the command).
# also, it's a good idea to check al sessions opened by screen command : "screen -ls"
######### HOW TO DEBUG #############################################################


#########   GOOG TO KNOW about the command screen ########################################



#########   GOOG TO KNOW about bash options  ########################################
# please, note that "-" will enable the feature and "+" will disable it ! for exemple :
# +e : will disable the errexit
# -e : will enable the errexit
#
# -e : errexit  : Abort script at first error, when a command exits with non-zero status  (except in until or while loops, if-tests, list constructs)
# -x : xtrace : Print each command to stdout before executing it, and, expands commands.
# -s : stdin  : Read commands from stdin
#https://www.tldp.org/LDP/abs/html/options.html
######### GOOG TO KNOW about bash options ############################################


######### GOOG TO KNOW about ssh/scp without password ###########################################
# Jenkins should not be waiting for password from stdin when running remote commands via ssh or scp. 
# so we need to configure jenkins machine to ssh other machines without password, to do so we
# have to add jenkins RSA public key to the authorized_keys of the target machine.
# i run this command on jenkins machine
# cat /home/ubuntu/.ssh/id_rsa.pub | ssh -i config_key.pem ubuntu@config.recette.myrhis.interne.fr "cat >> .ssh/authorized_keys"
# https://mikeeverhart.net/2013/05/adding-an-ssh-key-to-amazon-ec2/
# if you're using classic virtual machine you can use this command : ssh-copy-id. 
# https://askubuntu.com/questions/4830/easiest-way-to-copy-ssh-keys-to-another-machine
######### GOOG TO KNOW about ssh without password ###########################################


