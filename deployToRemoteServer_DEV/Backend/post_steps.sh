cd target

#delivery #Transfer the jar to the target server
scp -r *.jar dev@192.168.3.51:/home/dev/delivery

#Run java app in background on the target server 
ssh dev@192.168.3.51 bash +e -c "'
cd /home/dev/delivery
echo "java -Xms256m -Xmx2048m -jar $(ls | grep *.jar | head -n 1) --spring.profiles.active=dev" | at now + 1 min
'"
