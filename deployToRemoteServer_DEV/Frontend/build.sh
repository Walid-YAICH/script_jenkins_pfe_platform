pwd
echo $PATH

#Car le dist est sur git, c'est une mauvaise pratique, il faut le supprimer du repo
rm -rf dist

#Installer les dépendances du projet
npm install

#Construire le livrable PROD avec --prod
ng build #--prod #--base-href 

#Supprimer l'ancienne version du serveur cible
ssh dev@192.168.3.51 bash "
#c est le path surlequel pointe Nginx
cd /home/dev/delivery/angular_platforme_stages
rm -rf dist/
"

#Transférer l'application vers le serveur cible 
scp -r ./dist dev@192.168.3.51:/home/dev/delivery/angular_platforme_stages/