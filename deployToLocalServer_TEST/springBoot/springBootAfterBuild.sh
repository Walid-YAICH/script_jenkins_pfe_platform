#So "at now" will run even if java -jar fails
set +e

cd target

#Run java app in background
echo "java -Xms256m -Xmx2048m -jar $(ls | grep *.jar | head -n 1) --spring.profiles.active=test" | at now + 1 min
