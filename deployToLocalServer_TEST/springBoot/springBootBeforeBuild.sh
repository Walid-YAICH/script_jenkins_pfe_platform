set +e


#tester si l'application springboot est en cours d'exécution
fuser -v -n tcp 8090

#Tester si la dernière commande a fonctionnée normalement.
if [ $? -eq 0 ]
 then

   #Arrêtez l'application si elle est en cours d'exécution
   kill $(fuser -v -n tcp 8090)

 else 
   echo "processus already stopped :)"
fi