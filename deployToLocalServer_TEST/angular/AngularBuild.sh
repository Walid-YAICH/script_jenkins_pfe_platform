set +e

#chercher et remplacer "localhost" par l'adresse de l'application springBoot
sed -i 's/localhost/192.168.1.11/g' src/services/contacts.service.ts

#Installer les dépendances du projet
npm install

#tester si l'application est en cours d'exécution
fuser -v -n tcp 4200

#Tester si la dernière commande lancée a fonctionnée normalement
if [ $? -eq 0 ]
then

   #Arrêter l'application 
   kill $(fuser -v -n tcp 4200)

   #Lancer l'application avec sa nouvelle version
   echo "ng serve --host 0.0.0.0" | at now + 1 min
else
   echo "processus already stopped :)"
   echo "ng serve --host 0.0.0.0" | at now + 1 min
fi
